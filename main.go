package main

import (
	"assignment2/cmd"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

var StartTime = time.Now()

func main() {
	cmd.StartTime = StartTime
	
	// Variable used to make sure webhooks gets a unique ID.
	cmd.LastUsedId = -1

	port := "8080"

	if os.Getenv("PORT") != "" {
		port = os.Getenv("PORT")
	}
	if port == "" {
		log.Fatal("PORT must be specified")
	}

	http.HandleFunc("/repocheck/v1/commits/", cmd.CommitsHandler)
	http.HandleFunc("/repocheck/v1/languages/", cmd.LanguagesHandler)
	http.HandleFunc("/repocheck/v1/status/", cmd.StatusHandler)
	http.HandleFunc("/repocheck/v1/webhooks/", cmd.WebhooksHandler)
	http.HandleFunc("/repocheck/v1/webhooks", cmd.WebhooksHandler)
	http.HandleFunc("/", cmd.HomepageHandler)


	fmt.Println("Service listening on port " +  port)

	listenError := http.ListenAndServe(":" + port, nil)
	if listenError != nil{
		fmt.Println(fmt.Errorf("Error while listening and serving on port " + port + "\n"))
	}

}

