CloudAssignment2
** NOT IMPLEMENTED *** 

Authorization has not been implemented, so I can only retrieve about 189 repositories.

Webhooks database has not been implemented.

Issues handler has not been implemented because we only needed 2 out of the 3 services.

*** HOW TO USE THE PROGRAM ***

Firstly, the user has to be connected to the NTNU network since this is where the application is run. In other words, the app is not reachable externaly.

http://10.212.138.92:8080/repocheck/v1/languages/ Will retrieve the 5 most used languages as default, if not specified by a limiter ( e.g. /repocheck/v1/languages/8 ). The number is representing the number of occurences of that language found in the repositories, it does not count the code line percentage.

http://10.212.138.92:8080/repocheck/v1/commits/ Will return 5 repositories as default with the highest number of commits. if a limiter is specified ( e.g. /repocheck/v1/languages/8 ) it will return 8 repositories with highest commits.

http://10.212.138.92:8080/repocheck/v1/Status/ Will return the StatusCode from GitLab, uptime in seconds and the version.

http://10.212.138.92:8080/repocheck/v1/webhooks/ Will return every saved webhook.