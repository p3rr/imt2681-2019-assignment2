package cmd

import (
	"bytes"
	"encoding/json"
	"net/http"
	"strings"
	"time"
)

func QuickSort(items []repository) {							// Sorts a repository array by the amount of commits in each repo
	if len(items) > 1 {
		pivotIndex := len(items) / 2
		var smallerItems []repository
		var largerItems []repository

		for i := range items {
			repo := items[i]
			if i != pivotIndex {
				if repo.Commits > items[pivotIndex].Commits {
					smallerItems = append(smallerItems, repo)
				} else {
					largerItems = append(largerItems, repo)
				}
			}
		}

		QuickSort(smallerItems)
		QuickSort(largerItems)

		var merged = append(append(append([]repository{}, smallerItems...), []repository{items[pivotIndex]}...), largerItems...)

		for j := 0; j < len(items); j++ {
			items[j] = merged[j]
		}
	}
}

func CheckError(error error, writer http.ResponseWriter){					// Used to check errors
	if error != nil{
		http.Error(writer, error.Error(), http.StatusInternalServerError)
	}
}

func Uptime(starTime time.Time) time.Duration {								// Returns time since start of application
	return time.Since(starTime)
}

func sendToWebhooks(writer http.ResponseWriter, params int, event string){
	for _, object := range allWebhooks{										//For all webhooks
		if strings.ToUpper(object.Event) == event{							//If that webhook has the event from parameter
			var invocation = Invocation{Event: object.Event, Params: params, TimeStamp: time.Now()}		//Creates the struct to send

			data, marshalError := json.Marshal(invocation)					//Marshals this to json format
			CheckError(marshalError, writer)								//Checks for errors

			//Sends a porst request with URL, Contenttype and the struct as a buffer
			_, postError := http.Post(object.Url, "application/json", bytes.NewBuffer(data))
			CheckError(postError, writer)			//Checks for errors
		}
	}
}
