package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"
)

var perPageURL = "https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100/"
var pageURL = "https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&page="
var commitURL = "https://git.gvk.idi.ntnu.no/api/v4/projects/"
var languageURL = "https://git.gvk.idi.ntnu.no/api/v4/projects/"
var StartTime time.Time
var LastUsedId int
var allWebhooks []Webhooks

func CommitsHandler(writer http.ResponseWriter, r*http.Request){
	if r.Method == http.MethodGet{
		http.Header.Add(writer.Header(), "content-type", "application/json")
		URLParts := strings.Split(r.URL.Path, "/")
		if URLParts[1] == "repocheck" && URLParts[2] == "v1" && URLParts[3] == "commits"{	// Makes sure the intended URL is written
			_, err := strconv.Atoi(URLParts[4])		// Tries to transform the last part of the URL to a number (Which it is intended to be)
			if err == nil || URLParts[4] == "" {	// If the last part of the URL is a number or empty, the code runs, if not an error is displayed
				numberOfRepos := 0
				if URLParts[4] == "" { // Checks if the user has specified the amount of repos, otherwise default is 5
					numberOfRepos = 5
				} else {
					numberOfRepos, _ = strconv.Atoi(URLParts[4])
				}

				var repos = &[]repository{} // Creates an array of repositories
				for i := 1; i <= 2; i++ {   // Loops through each URL page of repos
					var temp = &[]repository{}
					response, err := http.Get(pageURL + strconv.Itoa(i)) // Uses api to get information about the repos from a certain page
					CheckError(err, writer)
					err = json.NewDecoder(response.Body).Decode(temp) // Fills the repo array with ID and names
					CheckError(err, writer)
					*repos = append(*repos, *temp...) // Appends the repos for each page to the repo array we are using
				}

				for i, object := range *repos { // Loops through all the repositories, i is the index in the loop, object is the struct object
					var numCommits = &[]numCommits{}
					repo, err := http.Get(commitURL + strconv.Itoa(object.ID) + "/repository/commits") // Gets all commits
					CheckError(err, writer)
					json.NewDecoder(repo.Body).Decode(numCommits) // Puts all commits for the given repo in array
					(*repos)[i].Commits = len(*numCommits)        // The number of commits for a certain repo is the same as the length of the numCommits array
				}

				QuickSort(*repos) // Sorts the repos

				highestCommits := make([]repository, numberOfRepos) // Creates an array with the highest commits repos

				for i := 0; i < numberOfRepos; i++ { // Fills the array with the given amount of repos
					highestCommits[i] = (*repos)[i]
				}

				encodeErr := json.NewEncoder(writer).Encode(highestCommits)
				CheckError(encodeErr, writer)

				//Invokes all the webhooks to this event
				sendToWebhooks(writer, numberOfRepos, "COMMITS")

			} else {http.Error(writer, "Not a number at the end of the URL", http.StatusBadRequest)}
		} else {http.Error(writer, "Malformed URL", http.StatusBadRequest)}
	} else { http.Error(writer, "not implemented yet", http.StatusNotImplemented) }
}

func LanguagesHandler(writer http.ResponseWriter, r*http.Request){
	if r.Method == http.MethodGet {
		http.Header.Add(writer.Header(), "content-type", "application/json")
		URLParts := strings.Split(r.URL.Path, "/")
		if URLParts[1] == "repocheck" && URLParts[2] == "v1" && URLParts[3] == "languages" { // Makes sure the intended URL is written
			_, err := strconv.Atoi(URLParts[4])		// Tries to transform the last part of the URL to a number (Which it is intended to be)
			if err == nil || URLParts[4] == ""{		// If the last part of the URL is a number or empty, the code runs, if not an error is displayed
				numberOfLanguages := 0
				if URLParts[4] == "" { // Checks if the user has specified the amount of languages, otherwise default is 5
					numberOfLanguages = 5
				} else {
					numberOfLanguages, _ = strconv.Atoi(URLParts[4])
				}
				var ProgLanguages map[string]float32 // Map for storing the language and percentage
				KeysCollection := make([]string, 0, len(ProgLanguages))
				var repo = &[]repository{} //var for saving the ID

				for i := 1; i <= 2; i++ { // Loops through each URL page of repos
					var temp = &[]repository{}
					response, err := http.Get(pageURL + strconv.Itoa(i)) //retrieves info from specified page number
					CheckError(err, writer)

					err = json.NewDecoder(response.Body).Decode(temp) // Fills the repo array with ID and names
					CheckError(err, writer)
					*repo = append(*repo, *temp...) // Appends into array for each page
				}

				//loop for retrieving languages for every project
				for _, elem := range *repo {
					response, err := http.Get(languageURL + strconv.Itoa(elem.ID) + "/languages")
					CheckError(err, writer)

					//reads into ProgLanguages
					err = json.NewDecoder(response.Body).Decode(&ProgLanguages)
					CheckError(err, writer)

					//loop for appending language for each repo into array
					for k := range ProgLanguages {
						KeysCollection = append(KeysCollection, k)
					}
				}

				//Counts the number of occurences from the KeysCollection array
				counter := make(map[string]int)
				for _, row := range KeysCollection {
					counter[row]++
				}

				var SortStruct []Language //var for saving the language key and frequency value
				for k, v := range counter {
					SortStruct = append(SortStruct, Language{k, v})
				}

				//Sorts the array of structs based on the frequency Value
				sort.SliceStable(SortStruct, func(i, j int) bool {
					return SortStruct[i].Value > SortStruct[j].Value
				})

				highestLanguages := make([]Language, numberOfLanguages) // Creates an array with the highest languages repos

				if numberOfLanguages <= len(SortStruct) {
					for i := 0; i < numberOfLanguages; i++ { // Fills the array with the given amount of repos
						highestLanguages[i] = (SortStruct)[i]
					}
					err := json.NewEncoder(writer).Encode(&highestLanguages)
					CheckError(err, writer)

					//Invokes all the webhooks to this event
					sendToWebhooks(writer, numberOfLanguages, "LANGUAGES")

				} else {http.Error(writer, "The requested amount of languages cannot exceed " + strconv.Itoa(len(SortStruct)), http.StatusBadRequest)}
			} else {http.Error(writer, "Not a number at the end of the URL", http.StatusBadRequest)}
		} else {http.Error(writer, "Malformed URL", http.StatusBadRequest)}
	} else { http.Error(writer, "not implemented yet", http.StatusNotImplemented) }
}

func StatusHandler(writer http.ResponseWriter, r*http.Request){
	if r.Method == http.MethodGet{
		http.Header.Add(writer.Header(), "content-type", "application/json")

		if r.URL.Path == "/repocheck/v1/status/"{
			URLParts := strings.Split(r.URL.Path, "/")
			var status = &Status{}

			testResponse, getError := http.Get(commitURL)		//GET request to test status
			CheckError(getError, writer)						//Check for errors

			status.GitLabStatus = testResponse.StatusCode		//Sets the error code, should be 200

			closeErr := testResponse.Body.Close()				//Closes the reponse
			CheckError(closeErr, writer)						//Check for errors

			status.UpTime = int(Uptime(StartTime)/1000000000)			//Gets uptime in seconds
			status.Version = URLParts[2]						//Sets version from client URL

			encodeErr := json.NewEncoder(writer).Encode(status)		//Encodes the struct to the client
			CheckError(encodeErr, writer)							//Check for errors

		} else { http.Error(writer, "Malformed URL", http.StatusBadRequest) }
	} else { http.Error(writer, "not implemented yet", http.StatusNotImplemented) }
}

func HomepageHandler(writer http.ResponseWriter, r*http.Request){
	if r.URL.Path == "/" {
		bytes, printError := fmt.Fprintf(writer, "What can you do on this page?\n\n"+
			" 1. You can get the top amount of commits from the available repos by using:\n"+
			"/repocheck/v1/commits/*AMOUNT OF REPOS*\n"+
			"The last part is optional, here you can define how big the output list shall be by writing a number.\n\n" +
			"2. You can get the top amount of languages used accross all repos by using:\n"+
			"/repocheck/v1/languages/*AMOUNT OF LANGUAGES*\n"+
			"This last part is also optional, and also defines how big the output list is.\n\n" +
			"3. To display active webhooks, use:\n"+
			"/repocheck/v1/webhooks/*SPECIFIC WEBHOOK*\n" +
			"Optionally, to view a specific webhook, enter its ID number in the last part of the URL.\n\n" +
			"4. To display the status of the service, use:\n" +
			"/repocheck/v1/status")

		CheckError(printError, writer)
		if bytes == 0{
			fmt.Println(fmt.Errorf("No text was written to page\n"))
		}

	} else {http.Error(writer, "Malformed URL", http.StatusBadRequest)}
}

func WebhooksHandler(writer http.ResponseWriter, reader*http.Request) {
	http.Header.Add(writer.Header(), "content-type", "application/json")

	switch reader.Method {

	//Reads information about a webhook for user, andre stores it in an array
	case http.MethodPost:
		if reader.URL.Path == "/repocheck/v1/webhooks" || reader.URL.Path == "/repocheck/v1/webhooks/"{
			newWebhook := &Webhooks{}
			decodeError := json.NewDecoder(reader.Body).Decode(newWebhook)		//Decodes information from user, to a struct
			CheckError(decodeError, writer)										//Checks for errors
			LastUsedId++														//Increases the ID for this webhook to use
			newWebhook.Id = LastUsedId											//Sets an ID for the webhook
			newWebhook.TimeStamp = time.Now()									//Sets a timestamp for when the webhook was added

			allWebhooks = append(allWebhooks, *newWebhook)						//Appens this struct to the array of all webhooks
			bytesWritten, printError := fmt.Fprintln(writer, len(allWebhooks)-1)		//Returns the ID of the wehook to the user

			CheckError(printError, writer)												//Checks for errors
			if bytesWritten == 0 {														//Checks if no bytes was written, then somwthing went wrong
				fmt.Println(fmt.Errorf("No text was written to page\n"))
			}

		} else { http.Error(writer, "Malformed URL", http.StatusBadRequest) }


	//GET request to print out webhooks
	case http.MethodGet:
		URLParts := strings.Split(reader.URL.Path, "/")
		if reader.URL.Path == "/repocheck/v1/webhooks/" {	//If user wants all webhooks
			encodeError := json.NewEncoder(writer).Encode(allWebhooks)		//Encode all webhooks
			CheckError(encodeError, writer)									//And check for error
			//If not, but user want one specific webhook
		} else if len(URLParts) == 5 && URLParts[1] == "repocheck" && URLParts[2] == "v1" && URLParts[3] == "webhooks"{

			webhookId, convertError := strconv.Atoi(URLParts[4])	//Get this ID to a string
			CheckError(convertError, writer)						//Check for errors

			if webhookId <= LastUsedId {												//If asked id exists
				for i, object := range allWebhooks { 									//Go through all webhooks
					if object.Id == webhookId { 										//When you find the webhook
						encodeError := json.NewEncoder(writer).Encode(allWebhooks[i]) //Encode this to user
						CheckError(encodeError, writer)                               //And check for errors
					}
				}		//If asked id does not exist, tell the user so
			} else {http.Error(writer, "Webhook does noe exist", http.StatusBadRequest)}
		}

	//Deletes the webhook with the id sendt from user
	case http.MethodDelete:
		if reader.URL.Path == "/repocheck/v1/webhooks" || reader.URL.Path == "/repocheck/v1/webhooks/"{

			bytesRead, readError := ioutil.ReadAll(reader.Body)				//Reads the body of the result as a byte
			CheckError(readError, writer)									//Checks for errors

			webhookId, convertError := strconv.Atoi(string(bytesRead))		//Converts from bytes to string, then to int
			CheckError(convertError, writer)								//Checks for errors

			if webhookId <= LastUsedId {									//If asked id to delete exists
				box := 0
				tempArray := make([]Webhooks, len(allWebhooks)-1)			//Create a temp array
				for i, object := range allWebhooks {						//Go through all webhooks
					if object.Id != webhookId {								//If the webhook is noe the one we want to delete
						tempArray[box] = allWebhooks[i]						//Then copy this to the first available box in temparray
						box++												//Then set the next avalable box for nest turn
					}
				}

				//When everyone are copyed (exept the one to delete), the global array is set to become the temparray
				allWebhooks = tempArray

				//Tells the user if the webhook was deletens
				bytesWritten, printError := fmt.Fprintln(writer, "Webhook with ID =", webhookId, "was deleted")

				CheckError(printError, writer)				//Checks for errors
				if bytesWritten == 0 {						//If nothing was written to user, then something went wrong
					fmt.Println(fmt.Errorf("No text was written to page\n"))
				}
				//Tells the user if the webhook does noe exist
			} else {http.Error(writer, "Webhook does noe exist", http.StatusBadRequest)}
		}


	default:		//Tells the user that this action is noe available
		http.Error(writer, "Invalid method "+reader.Method, http.StatusBadRequest)
	}
}