package cmd

import "time"

// COMMITS

// Represents a single repository
type repository struct{
	ID 		int `json:"id"`
	Name    string `json:"path_with_namespace"`
	Commits int
}

// The number of commits for a repo
type numCommits struct{
	Commits int `json:"id"`
}

//LANGUAGES

// Represents a language
type Language struct{
	Key string
	Value int
}


// STATUS

type Status struct {
	GitLabStatus	int 	// indicates whether gitlab service is available based on HTTP status code
	UpTime			int		// seconds since service start
	Version 		string	//Status
}

type Webhooks struct{
	Id			int
	Event		string			`json:"event"`
	Url			string			`json:"url"`
	TimeStamp	time.Time
}

type Invocation struct{
	Event	string	`json:"eventtype"`		// event type as per specification for registration
	Params	int							// parameters passed along in triggering request
	TimeStamp time.Time						// timestamp of request
}